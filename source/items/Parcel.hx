package items;

import Item;
import flixel.util.FlxColor;

class Parcel extends Item {

    private var image:String = "parcel";

    public function new(X:Float=0, Y:Float=0) {

        super(X,Y);

        makeGraphic(15,15,FlxColor.BLUE);
    }

}