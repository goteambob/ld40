

package;

import flixel.FlxSprite;
import flixel.util.FlxColor;

class Belt extends FlxSprite {
    
    private var x_width:Int;
    private var y_width:Int;
    
    public function new(X:Float=0, Y:Float=0, isIn:Bool = true ) {
            
            super(X,Y);

            if (isIn) {
                x_width = 800;
                y_width = 40;
            } else {
                x_width = 40;
                y_width = 400;
            }


            makeGraphic(x_width,y_width,FlxColor.WHITE);



    }    
    
}