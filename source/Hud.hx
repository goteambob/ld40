 package;

 import flixel.FlxSprite;
 import flixel.group.FlxGroup.FlxTypedGroup;
 import flixel.text.FlxText;
 import flixel.util.FlxColor;

 class Hud extends FlxTypedGroup<FlxSprite>
 {
     
     private var _txtLevel:FlxText;
     

     public function new()
     {
         super();        
         _txtLevel = new FlxText(16, 2, 0, "Level: 0", 8);
         _txtLevel.setBorderStyle(SHADOW, FlxColor.GRAY, 1, 1);         
         add(_txtLevel);         
     }

     public function updateHUD(level:Int):Void
     {
         _txtLevel.text = "Level: " + Std.string(level);
     }
 }