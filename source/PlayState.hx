package;

import flixel.FlxState;
import flixel.FlxG;
import flixel.group.FlxGroup;

class PlayState extends FlxState
{

	private var _inBelt:Belt;
	private var _outBelts = new List<Belt>();

	private var _items:FlxTypedGroup<Item>;


	private var level:Int = 1;
	private var timePassed:Float = 0;

	private var hud:Hud;

	private var player:Player;
	

	override public function create():Void
	{

		_inBelt = new Belt(20,20);
		add(_inBelt);

		// Add 1 belt to start with

		var belt1 = new Belt(300,300, false);		
		_outBelts.add(belt1);

		for (belt in _outBelts) {
			add(belt);
		}		

		var player = new Player(100,100);
		add(player);

		_items = new FlxTypedGroup<Item>();
		addRandomItem();
		

		hud = new Hud();		
		add(hud);

		super.create();
	}

	override public function update(elapsed:Float):Void
	{
		super.update(elapsed);
		
		if (_items != null) {
			FlxG.overlap(player,_items, test);
		}


		timePassed += elapsed;
		if (timePassed > 20 - level) {

			timePassed = 0;
			level = level + 1;

			hud.updateHUD(level);

			addRandomItem(100 + level);

		}
		FlxG.log.notice(elapsed);
	}

	private function test(P:Player, I:Item):Void {
		FlxG.log.notice("BING");
	}

	public function addRandomItem(speed:Int = 100) {

		// Just add a new parcel for now.
		var parcel = new items.Parcel(_inBelt.x - 10, _inBelt.y + (_inBelt.y/2));
		parcel.moveSpeed = speed;
		_items.add(parcel);
		add(parcel);

		FlxG.log.notice("Added new Parcel");

	}
}
