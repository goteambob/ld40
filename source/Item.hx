package;

import flixel.FlxSprite;

class Item extends FlxSprite {

    public var moveSpeed:Float = 100;
    private var pickedUp:Bool = false;    

    public function new (?X:Float=0, ?Y:Float=0) {
        super(X,Y);
        setSize(15, 15);
    }

    override public function update(elapsed:Float):Void {

        
        if (!pickedUp) {        
            velocity.x = 15;
        }            

        super.update(elapsed);

    }

}